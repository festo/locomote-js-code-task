'use strict';

$(document).ready(function () {

  $('#dateInput').datepicker({
    format: 'yyyy-mm-dd',
  });

  // Ato suggest
  $('input.typeahead').typeahead({
    onSelect: function (item, element) {
      element.data('airportCode', item.value);
    },
    ajax: {
      method: 'get',
      url: '/api/airports',
      triggerLength: 2,
      displayField: 'airportName',
      valueField: 'airportCode',
      preDispatch: function (query) {
        return {
          q: query,
        };
      },
      preProcess: function (result) {
        if (!!result.error) {
          // Hide the list, there was some error
          return false;
        }
        // We good!
        return result.data;
      },
    }
  });

  $('.search-btn').on('click', function () {
    let from = $('#fromInput').data('airportCode');
    let to = $('#toInput').data('airportCode');
    let date = $('#dateInput').val();

    let queryParams = {
      from: from,
      to: to,
      date: date,
    };

    let $results = $('.results');
    $results.html('Loading ...');

    $.ajax({
      type: "POST",
      url: '/api/search',
      // type: 'GET',
      // url: '/test-response.json',
      data: queryParams,
      dataType: 'json'
    })
      .done(function (result) {
        if (!!result.error) {
          $results.html(result.error);
          return;
        }

        $results.html($('.tab-template').html());
        result.data.forEach(function (value) {
          let active = value.date === queryParams.date;
          let tab = $('<li></li>').attr({
            role: 'presentation',
            //class: 'active',
          });

          if (active) {
            tab.addClass('active');
          }

          tab.html('<a href="#' + value.date + '" aria-controls="home" role="tab" data-toggle="tab">' + value.date + '</a>');
          $results.find('.tablist').append(tab);

          let panel = $('<div></div>').attr({
            role: 'tabpanel',
            class: 'tab-pane',
            id: value.date,
          });

          if (active) {
            panel.addClass('active');
          }

          panel.html($('.table-template').html());
          let table = panel.find('table');
          let tbody = table.find('tbody');

          value.flights.forEach(function (flight) {
            let row = $('<tr></tr>');
            row.append('<td>' + flight.airline.name + '</td>');
            row.append('<td>' + flight.flightNum + '</td>');
            row.append('<td>' + flight.start.dateTime + '</td>');
            row.append('<td>' + flight.finish.dateTime + '</td>');
            row.append('<td>' + flight.durationMin + '</td>');
            row.append('<td>' + flight.price + '</td>');
            tbody.append(row);
          });

          $results.find('.tab-content').append(panel);
        });
      })
      .fail(function () {
        $results.html('Something went wrong');
      });

    console.log(queryParams);
  });
});