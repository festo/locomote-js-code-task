'use strict';

module.exports = {
  airlines: require('./airlines.js'),
  airports: require('./airports.js'),
  search: require('./search.js'),
};
