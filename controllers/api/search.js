'use strict';

const wrap = require('co-express');
const co = require('co');
const appRoot = require('app-root-path');
const moment = require('moment');

const FlightAPI = require(appRoot + '/services/FlightAPI.js');

const Response = require(appRoot + '/services/response.js');


// Handling the transfers without any database is a pain in the ass, the API only send back flights witout transit
module.exports = wrap(function*(req, res) {
  let response = new Response();
  let keys = ['from', 'to', 'date'];

  // Check parameters
  for (let key of keys) {
    if (!req.body[key]) {
      response.error('Missing parameter');
      return res.json(response.send());
    }
  }

  if (!moment(req.body.date).isValid()) {
    response.error('Invalid parameter');
    return res.json(response.send());
  }

  let airlines = yield FlightAPI.airlines();

  let results = [];
  // For the time frame
  for (let i = -2; i <= 2; i++) {
    let date = moment(req.body.date).add(i, 'day').format('YYYY-MM-DD');
    let flights = yield airlines.map((airline) => {
      let params = {
        date: date,
        from: req.body.from,
        to: req.body.to,
        airlineCode: airline.code,
      };
      return FlightAPI.search(params);
    });

    // Flatten the array
    flights = [].concat.apply([], flights);

    // Reduce response data
    flights = flights.map(function (flight) {
      return {
        airline: {
          name: flight.airline.name
        },
        flightNum: flight.flightNum,
        start: {
          dateTime: flight.start.dateTime,
        },
        finish: {
          dateTime: flight.finish.dateTime,
        },
        durationMin: flight.durationMin,
        price: flight.price,
      }
    });

    results.push({
      date: date,
      flights: flights,
    });
  }

  response.data(results);
  res.json(response.send());

});
