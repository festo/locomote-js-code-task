'use strict';

const appRoot = require('app-root-path');

const FlightAPI = require(appRoot + '/services/FlightAPI.js');
const Response = require(appRoot + '/services/response.js');

module.exports = function(req, res) {
  let query = req.query.q;
  let response = new Response();
  FlightAPI
    .airports(query)
    .then(function(result) {
      response.data(result);
    })
    .catch(function(err) {
      response.error(err);
    })
    .then(function() {
      res.json(response.send());
    });
};
