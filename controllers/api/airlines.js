const wrap = require('co-express');
const appRoot = require('app-root-path');

const FlightAPI = require(appRoot + '/services/FlightAPI.js');
const Response = require(appRoot + '/services/response.js');

module.exports = wrap(function*(req, res) {
  let airlines = yield FlightAPI.airlines();
  let response = new Response().data(airlines).send();
  res.json(response);
});
