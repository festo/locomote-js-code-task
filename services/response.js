'use strict';

module.exports = class Response {
  constructor() {
    this.response = {
      error: null,
      data: null,
    };
  }

  data(data) {
    this.response.data = data;
    return this;
  }

  error(message) {
    this.response.error = message;
    return this;
  }

  send() {
    return this.response;
  }
};
