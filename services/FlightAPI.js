'use strict';

const request = require('request');
const moment = require('moment');
const logger = require('./logger');

const baseUri = 'http://node.locomote.com/code-task/';

module.exports = {
  airlines: function() {
    return new Promise(function(resolve) {
      request.get({url: baseUri + 'airlines', json:true}, function(err, result, body) {
        if(err) {
          logger.error(err);
          // send back an empty array
          return resolve([]);
        }

        if(result.statusCode !== 200) {
          return reject('Invalid query parameter');
        }

        // I can pass it directly because of the json:true options
        return resolve(body);
      });
    });
  },

  airports: function(filter) {
    return new Promise(function(resolve, reject) {
      let qs = {
        q: filter,
      };
      request.get({url: baseUri + 'airports', qs:qs, json:true}, function(err, result, body) {
        if(err) {
          logger.error(err);
          return reject();
        }

        // Invalid query parameter
        if(result.statusCode !== 200) {
          return reject('Invalid query parameter');
        }

        // I can pass it directly because of the json:true options
        return resolve(body);
      });
    });
  },

  search: function(filter) {
    return new Promise(function(resolve, reject) {
      let keys = ['airlineCode', 'from', 'to', 'date'];

      // Check parameters
      for(let key of keys) {
        if(!filter.hasOwnProperty(key)) {
          return reject('Missing argument');
        }
      }

      let qs = {
        date: moment(filter.date).format('YYYY-MM-DD'),
        from: filter.from,
        to: filter.to,
      };
      request.get({url: baseUri + 'flight_search/' + filter.airlineCode, qs:qs, json:true}, function(err, result, body) {
        if(err) {
          logger.error(err);
          return reject();
        }

        // Oups! The server throws an 500 if we pass an invalid argument
        if(result.statusCode !== 200) {
          return reject(body);
        }

        // I can pass it directly because of the json:true options
        return resolve(body);
      });
    });
  },
};
