'use strict';

const chai = require('chai');
const chaiHttp = require('chai-http');
const expect = chai.expect;

chai.use(chaiHttp);

const baseURL = 'http://localhost:3000/api';
const API = chai.request(baseURL);

describe('Local API', function () {
  describe('#airlines', function() {
    it('should send back airlines', function(done) {
      API
        .get('/airlines')
        .end(function (err, res) {
          expect(err).to.be.null;
          expect(res).to.have.status(200);
          done();
        });
    });
  });

  describe('#airports', function() {
    describe('with valid parameters', function() {
      it('should send back airports', function(done) {
        API
          .get('/airports')
          .query({
            q: 'Melbourne'
          })
          .end(function (err, res) {
            expect(err).to.be.null;
            expect(res).to.have.status(200);
            expect(res.body).to.have.property('data');
            expect(res.body.data.length).to.be.eq(2);
            done();
          });
      });
    });

    describe('without parameters', function() {
      it('should send back error', function(done) {
        API
          .get('/airports')
          .end(function (err, res) {
            expect(err).to.be.null;
            expect(res).to.have.status(200);
            expect(res.body).to.have.property('error');
            expect(res.body.error).to.be.not.eq('');
            done();
          });
      });
    });
  });

  describe('#search', function() {
    it('should send back flights', function(done) {
      // The response is really slow
      this.timeout(60000);
      API
        .post('/search')
        .send({
          to: 'JFK',
          from: 'SYD',
          date: '2018-09-02'
        })
        .end(function (err, res) {
          expect(err).to.be.null;
          expect(res).to.have.status(200);
          expect(res.body).to.have.property('data');
          expect(res.body.data.length).to.be.eq(5);
          done();
        });
    });
  });
});