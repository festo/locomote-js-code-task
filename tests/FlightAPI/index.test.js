'use strict';

const appRoot = require('app-root-path');
const chai = require('chai');
const expect = chai.expect;

const FlightAPI = require(appRoot + '/services/FlightAPI');

describe('Flight API', function () {
  describe('#airlines()', function () {
    it('should return a success response', function (done) {
      FlightAPI
        .airlines()
        .then(function (airlines) {
          if (Array.isArray(airlines)) {
            done();
          } else {
            done(new Error('airlines is not a array'));
          }
        })
        .catch(done);
    });
  });

  describe('#airports()', function () {
    describe('With valid param', function () {
      it('should return a success response', function (done) {
        FlightAPI
          .airports('Melbourne')
          .then(function (airports) {
            expect(airports.length).be.eq(2);
            done();
          })
          .catch(done);
      });
    });

    describe('with invalid param', function () {
      it('should return an error', function (done) {
        FlightAPI
          .airports('')
          .then(function () {
            done(new Error('We get a success response instead of error'));
          })
          .catch(function () {
            // Error is expected
            return done();
          });
      });
    });
  });

  describe('#search()', function() {
    describe('with valid params', function() {
      it('should return success response', function (done) {
        let filter = {
          from: 'SYD',
          to: 'JFK',
          date: '2018-09-02',
          airlineCode: 'QF'
        };

        FlightAPI
          .search(filter)
          .then(function (flights) {
            expect(flights.length).be.gt(0);
            done();
          })
          .catch(done);
      });
    });

    describe('with invalid params', function() {
      it('should return an error', function (done) {
        // date is missing
        let filter = {
          from: 'SYD',
          to: 'JFK',
          airlineCode: 'QF'
        };

        FlightAPI
          .search(filter)
          .then(function () {
            done(new Error('We get a success response instead of error'));
          })
          .catch(function () {
            // Error is expected
            return done();
          });
      });
    });

    describe('with invalid params', function() {
      it('should return an error', function (done) {
        // From is invalid
        let filter = {
          from: 'SZE',
          to: 'JFK',
          airlineCode: 'QF',
          date: '2018-09-02',
        };

        FlightAPI
          .search(filter)
          .then(function () {
            done(new Error('We get a success response instead of error'));
          })
          .catch(function () {
            // Error is expected
            return done();
          });
      });
    });
  });
});