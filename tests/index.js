'use strit';

// Main entry point
before('Start the server', function(done) {
  const server = require('../server.js');
  // Wait for the server
  server.on('start', done);
});