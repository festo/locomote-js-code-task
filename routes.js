'use strict';

const express = require('express');
const router = express.Router();

const homeController = require('./controllers/home');
const apiController = require('./controllers/api');

const errorHandler = require('./middleware/errorHandler.js');

router.get('/', homeController.home);

router.get('/api/airlines', apiController.airlines, errorHandler);
router.get('/api/airports', apiController.airports, errorHandler);
router.post('/api/search', apiController.search, errorHandler);

module.exports = router;