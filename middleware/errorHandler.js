'use strict';

const appRoot = require('app-root-path');
const Response = require(appRoot + '/services/response.js');
const logger = require(appRoot + '/services/logger');


module.exports = function(err, req, res) {
  logger.error(err);
  let response = new Response.error('Something went wrong').send();
  res.status(500);
  res.json(response);
};